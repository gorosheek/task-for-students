using Chat.Database;
using Chat.Dtos;
using Chat.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Chat.Controllers;

[Route("/chatrooms")]
[ApiController]
public class ChatroomController : ControllerBase
{
    private readonly ChatContext _chatContext;
    public ChatroomController(ChatContext chatContext){
        _chatContext = chatContext;
    }

    [HttpPost]
    public ActionResult AddChatroom(ChatroomDto chatroomDto){
        Chatroom chatroom = new Chatroom(){
            Title = chatroomDto.Title,
            Users = chatroomDto.UserIds.Select(id => _chatContext.Users.Find(id)).ToList()
        };

        _chatContext.Add(chatroom);
        _chatContext.SaveChanges();
        return Created(string.Empty, null);
    }

    [HttpGet]
    public ActionResult GetChatrooms(){
        var chatrooms = _chatContext.Chatroom.Include(u => u.Users).ToList();
        return Ok(chatrooms);
    }

    [HttpPatch]
    public ActionResult UpdateChatrooms(string title, int id){
        var chatroom = _chatContext.Chatroom.Include(u => u.Users).First(r => r.Id == id);

        chatroom.Title = title;
        _chatContext.Update(chatroom);
        _chatContext.SaveChanges();
        return Ok(chatroom);
    }
    
    [HttpDelete] 
    public ActionResult DeleteChatroom(int id){
        var chatroom = _chatContext.Chatroom.Include(u => u.Users).First(r => r.Id == id);

        chatroom.Deleted = true;
        _chatContext.Update(chatroom);
        _chatContext.SaveChanges();
        return Ok(chatroom);
    }
}