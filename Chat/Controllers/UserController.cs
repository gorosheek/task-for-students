using Chat.Database;
using Chat.Dtos;
using Chat.Models;
using Microsoft.AspNetCore.Mvc;

namespace Chat.Controllers;

[Route("/users")]
[ApiController]
public class UserController : ControllerBase
{
    private readonly ChatContext _chatContext;
    public UserController(ChatContext chatContext ){
        _chatContext = chatContext;
    }


    [HttpPost]
    public ActionResult AddUser(UserDto userDto){
        User user = new User() {
            Name = userDto.Name
        };
        _chatContext.Add(user);
        _chatContext.SaveChanges();
        return Created(string.Empty, null);
    }

    [HttpGet]
    public ActionResult GetUsers(){
        var users = _chatContext.Users.ToList();
        return Ok(users);
    }

    [HttpPatch]
    public ActionResult UpdateUserName(string name, int id){
        var user = _chatContext.Users.First(u => u.Id == id);

        user.Name = name;
        _chatContext.Update(user);
        _chatContext.SaveChanges();
        return Ok(user);
    }

    [HttpDelete]
    public ActionResult DeleteUser(int id){
        var user = _chatContext.Users.First(u => u.Id == id);
        user.Deleted = true;

        _chatContext.Update(user);
        _chatContext.SaveChanges();
        return Ok(user);
    }
}
