using Chat.Database;
using Chat.Dtos;
using Chat.Models;
using Microsoft.AspNetCore.Mvc;

namespace Chat.Controllers;

[Route("/messages")]
[ApiController]
public class MessageController : ControllerBase
{
    private readonly ChatContext _chatContext;
    public MessageController(ChatContext chatContext){
        _chatContext = chatContext;
    }

    [HttpPost]
    public ActionResult AddMessage(MessageDto messageDto){
        ChatMessage message = new ChatMessage(){
            Text = messageDto.Text
        };

        _chatContext.Add(message);
        _chatContext.SaveChanges();
        return Created(string.Empty, null);
    }

    [HttpGet]
    public ActionResult GetMessages(){
        var messages = _chatContext.ChatMessages.ToList();
        return Ok(messages);
    }
}