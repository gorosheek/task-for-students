namespace Chat.Dtos;

public class UserDto
{
    public string Name { get; set; }
    public string Login { get; set; }
}