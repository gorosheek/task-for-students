namespace Chat.Dtos;

public class ChatroomDto
{
    public string Title { get; set; }
    public IEnumerable<int> UserIds { get; set; }
}