using Chat.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Chat.Database.Configurations;

public class ChatroomConfiguration : IEntityTypeConfiguration<Chatroom>
{
    public void Configure(EntityTypeBuilder<Chatroom> builder)
    {
        builder.Property(r => r.Title).IsRequired();
        builder.Property(r => r.Deleted).HasDefaultValue(false);
        builder.HasMany(r => r.Users).WithMany();
    }
}